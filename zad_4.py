# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 14:40:33 2015

@author: matej
"""
unos=0
zbroj=0
brojac=0
ispad="Done"                        #uvjet ispadanja iz petlje
while True:
    unos=input("Upisite broj: ")
    if(str(unos) == ispad):         #ako je unesena rijec Done
        break
    else:
        try:                        #ako se radi o broju
            vr=float(unos)          #pretvaranje u float
            if(brojac==0):          #ako je ovo prvi uneseni broj
                mini=vr             #pocetna vrijednost min. i max. broja
                maxi=vr
            if(vr<mini):            #ako se ne radi o prvom broju usporeduje
                mini=vr             #trenutne vrijednosti min i max s novim
            if(vr>maxi):            #unesenim brojem
                maxi=vr
            zbroj+=vr               
            brojac+=1
        except ValueError:          #hvata pojedinacna slova i stringove
            print("Neispravan unos!")

print("Aritmeticka sredina je ", zbroj/brojac)  
print("Najmanji broj je ", mini, "\nNajveci broj je ", maxi)
        
        

