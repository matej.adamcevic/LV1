# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 22:40:19 2015

@author: matej
"""
niz_vr=[] #niz za pohranu citanih vrijednosti  
file = input('Enter the file name: ')  

try:
    fhand = open(file)
except:                    #ako je ime krivo upisano ili dat. nema u direktoriju
    print ('File cannot be opened:', file)
    exit()

for line in fhand:   
    if line.startswith('X-DSPAM-Confidence:'): #trazenje retka koji sadrzi trazeni podatak o pouzdanosti
        vr=float(line.split(':')[1]) #redak se prepolovi kod dvotocke i uzima desni dio -[1]
        niz_vr.append(vr)               #u niz se dodaje procitana vrijednost
print('Srednja vrijednost pouzdanosti: ',sum(niz_vr)/len(niz_vr)) #aritm. sredina podataka

