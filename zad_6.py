# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 23:51:55 2015

@author: matej
"""

niz_vr=[] #niz za pohranu citanih vrijednosti  
file = input('Enter the file name: ')  

try:
    fhand = open(file)
except:                    #ako je ime krivo upisano ili dat. nema u direktoriju
    print ('File cannot be opened:', file)
    exit()

hosts=dict()
for line in fhand:
    if line.startswith("From"):
        part=line.split('@')[1]     #uzima se dio reda(line) desno od @
        hname=part.split(' ')[0]    #uzima se dio dijela(part) lijevo od sljedeceg razmaka
        if(hname not in hosts):     #ako domene nema u rijecniku dodaj ju
            hosts[hname]=1
        else:                       #ako ima, inkrementiraj broj pojavljivanja
            hosts[hname]+=1
b=3                             #b je brojac za ispisivanje "nekoliko", tj. b domena
for hnm in hosts:
    if(b==0):
        break
    print("Domena ", hnm, "se pojavljuje ", hosts[hnm], "puta")
    b-=1
        
            